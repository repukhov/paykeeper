<?php

/**
 * @version 1.0
 *
 * @property string $login
 * @property string $password
 * @property string $keyAuthBasic
 * @property string $server
 * @property string $token
 *
 */
class PayKeeper{

	private $login = null;
	private $password = null;
	private $keyAuthBasic = null;
	private $server = null;
	private $token = null;

	# инициализируем данные
	public function __construct($login, $password, $server){
		$this->login = $login;
		$this->password = $password;
		# получаем ключ для Basic авторизации запросов
		$this->keyAuthBasic = base64_encode("$login:$password");
		$this->server = $server;
	}
	/**
	 * POST-запрос к ЛК PayKeeper
	 *
	 * @param string URI метода
	 * @param array массив с данными POST-запроса
	 * @return array
	 */
	private function Post($uri, $dataPost){
		# проверяем Basic ключ
		if(is_null($this->keyAuthBasic)){
			return false;
		}
		# подготавливаем заголовки
		$headers = array();
		array_push($headers,'Content-Type: application/x-www-form-urlencoded');
		array_push($headers,'Authorization: Basic '.$this->keyAuthBasic);
		# 6.1. Получение токена безопасности
		if(is_null($this->token)){
			# отправляем GET запрос на токен
			$result = $this->Get('/info/settings/token/');
			if (isset($result['token'])){
				$this->token = $result['token'];
			}else{
				return $result;
			}
		}
		# подготавливаем данные для post-запроса
		$data = http_build_query(array_merge($dataPost, array('token' => $this->token)));
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_URL, $this->server.$uri);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$out = curl_exec($curl);
		if(curl_errno($curl)){
    		return array(
    			'result' => 'fail',
    			'details' => 'Ошибка curl: ' . curl_error($curl)
    		);
		}
		# возвращаем результат в массиве
		return json_decode($out, true);
	}
	/**
	 * GET-запрос к ЛК PayKeeper
	 *
	 * @param string URI метода
	 * @param array массив с данными GET-запроса
	 * @return array
	 */
	private function Get($uri, $dataGet = array()){
		# проверяем Basic ключ
		if(is_null($this->keyAuthBasic)){
			return false;
		}
		# подготавливаем заголовки
		$headers = array();
		array_push($headers,'Content-Type: application/x-www-form-urlencoded');
		array_push($headers,'Authorization: Basic '.$this->keyAuthBasic);
		# подготавливаем данные для get-запроса
		if(count($dataGet) == 0){
			$data = '';
		}else{
			$data = '?'.http_build_query($dataGet);
		}
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_URL, $this->server.$uri.$data);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$out = curl_exec($curl);
		if(curl_errno($curl)){
    		return array(
    			'result' => 'fail',
    			'details' => 'Ошибка curl: ' . curl_error($curl)
    		);
		}
		# возвращаем результат в массиве
		return json_decode($out, true);
	}
	/**
	* 1.1. Запрос получения списка платежных систем
	*
	* @return array
	*/
	final public function SystemsList(){
		return $this->Get('/info/systems/list/');
	}
	/**
	* 1.2. Запрос получения баланса платежных систем
	*
	* @param array
	* @return array
	*/
	final public function SystemsSums($dataGet){
		return $this->Get('/info/systems/sums/');
	}
	/**
	* 1.3. Запрос получения баланса платежных систем, разбитого по
	* временным интервалам
	*
	* @param array
	* @return array
	*/
	final public function SystemsSumsDetails($dataGet){
		return $this->Get('/info/systems/sums/details/');
	}
	/**
	* 1.4. Запрос получения суммарного баланса всех платежных систем
	*
	* @param array
	* @return array
	*/
	final public function SystemsAllsum($dataGet){
		return $this->Get('/info/systems/allsum/');
	}
	/**
	* 2.1 Запрос получения реестра платежей
	*
	* @param array array('status' => array('','',''),'start' => '2016-04-14', и т.д);
	* @return array
	*/
	final public function PaymentsByDate($dataGet){
		return $this->Get('/info/payments/bydate/', $dataGet);
	}
	/**
	* 2.2 Запрос получения количества платежей за период
	*
	* @param array array('status' => array('','',''),'start' => '2016-04-14', и т.д);
	* @return array
	*/
	final public function PaymentsByDateCount($dataGet){
		return $this->Get('/info/payments/bydatecount/', $dataGet);
	}
	/**
	* 2.3. Запрос получения информации о платеже по идентификатору
	*
	* @param string номер платежа
	* @return array
	*/
	final public function PaymentByPaymentId($paymentId){
		return $this->Get('/info/payments/byid/', array('id' => $paymentId));
	}
	/**
	* 2.4. Запрос получение дополнительной информации об опциях
	*
	* @param string номер платежа
	* @return array
	*/
	final public function OptionsByPaymentId($paymentId){
		return $this->Get('/info/options/byid/', array('id' => $paymentId));
	}
	/**
	* 2.5. Запрос получения дополнительных необязательных параметров платежа
	*
	* @param string номер платежа
	* @return array
	*/
	final public function ParamsByPaymentId($paymentId){
		return $this->Get('/info/params/byid/', array('id' => $paymentId));
	}
	/**
	* 2.6. Запрос получения HTTP параметров платежа
	*
	* @param string номер платежа
	* @return array
	*/
	final public function HttpLogByPaymentId($paymentId){
		return $this->Get('/info/httplog/byid/', array('id' => $paymentId));
	}
	/**
	* 2.7. Запрос информации по возвратам для платежа
	*
	* @param string номер платежа
	* @return array|NULL
	*/
	final public function RefundsByPaymentId($paymentId){
		return $this->Get('/info/refunds/bypaymentid/', array('id' => $paymentId));
	}
	/**
	* 2.8. Запрос на возврат платежа
	* Признак частично возврата должен быть обязательно строкой 'true'.
	* При полном возврате корзина не нужна, только сумма и id платежа (чек напечатается и корзина
	* в чек попадёт корректная).
	* При частичном возврате корзина нужна обязательно (нужно для коректного отображения в чеке),
	* в которой будет только товар, по которому надо сделать возврат прихода. Общая сумма корзины
	* должна равняться сумме возврата, т.е. надо подогнать параметры корзины под сумму возврата и
	* указать сумму возврата amount(иначе сумма возврата не отразится в платеже)
	* Пример полного возврата:
	* 		array('id' => '<номер платежа>', 'amount' => 30);
	* Пример част. возврата:
	* 		array('id' => '<номер платежа>', 'amount' => 30, 'refund_cart' => '<корзина>', partial' => 'true');
	* Если нужно сделать полный возврат, после хотябы одиного частичного(ых) возврата,то все товары
	* указываем в корзине и указываем их общую сумму в $amount.
	* можно сделать только сразу! Иначе только частичными придётся возвращать с указанием корзины.
	* (если не передать корзину при этом, то чека не будет)
	*
	* @param array
	* @return array
	*/
	final public function ChangePaymentRevers($dataPost){
		return $this->Post('/change/payment/reverse/', $dataPost);
	}
	/**
	* 2.9. Запрос на сброс счетчика повторов для платежа
	* Его можно сбросить если он был установлен, т.е. у него
	* должно появится счётчик в параметрах платежа
	*
	* @param string $paymentId
	* @return array
	*/
	final public function ChangePaymentRepeatCnt($paymentId){
		return $this->Post('/change/payment/repeatcnt/', array( 'id' => $paymentId ));
	}
	/**
	* 2.11. Поиск платежей по значениям параметров в диапазоне дат
	*
	* @param array array('start' => '2016-04-14', и т.д);
	* @return array
	*/
	final public function InfoPaymentsSearch($dataGet){
		return $this->Get('/info/payments/search/', $dataGet);
	}
	/**
	* 2.12. Списание средств по ранее проведённой авторизации
	*
	* @param string $paymentId
	* @return array
	*/
	final public function ChangePaymentCapture($paymentId){
		return $this->Post('/change/payment/capture/', array( 'id' => $paymentId ));
	}
	/**
	* 2.13. Генерация чека окончательного расчёта
	*
	* @param string $paymentId
	* @return array
	*/
	final public function ChangePaymentPostSaleReceipt($paymentId){
		return $this->Post('/change/payment/post-sale-receipt/', array( 'id' => $paymentId ));
	}
	/**
	* 3.1. Запрос получения данных счёта
	*
	* @param string $paymentId
	* @return array
	*/
	final public function InfoInvoiceByInvoiceId($invoiceId){
		return $this->Get('/info/invoice/byid/', array('id' => $invoiceId));
	}
	/**
	* 3.2. История счетов(список счетов за указанный период)
	* для параметра status, если значений его много, то
	* можно передать в виде массива:
	*
	* @param array array('status' => array('','',''), 'form' => '', и т.д);
	* @return array
	*/
	final public function InfoInvoiceList($dataGet){
		return $this->Get('/info/invoice/list/', $dataGet);
	}
	/**
	* 3.3. История счетов(возвращает количество счетов за указанный период)
	* для параметра status, если значений его много, то
	* можно передать в виде массива:
	*
	* @param array array('status' => array('','',''), 'form' => '', и т.д);
	* @return array
	*/
	final public function InfoInvoiceListСount($dataGet){
		return $this->Get('/info/invoice/listcount/', $dataGet);
	}
	/**
	* 3.4. Подготовка счёта(создание)
	* Для создания и отправки используется два разных метода
	*
	* @param array $dataPost
	* @return array
	*/
	final public function ChangeInvoicePreview($dataPost){
		return $this->Post('/change/invoice/preview/', $dataPost);
	}
	/**
	* 3.5. Отправка счёта клиенту
	*
	* @param string $invoiceId
	* @return array
	*/
	final public function ChangeInvoiceSend($invoiceId){
		return $this->Post('/change/invoice/send/', array('id' => $invoiceId));
	}
	/**
	* 4.1. Запрос настроек текущего пользователя
	*
	* @return array
	*/
	final public function InfoUserSettings(){
		return $this->Get('/info/user/settings/');
	}
	/**
	* 4.2. Запрос информации о последнем посещении пользователя
	*
	* @return array
	*/
	final public function InfoLastVisit(){
		return $this->Get('/info/user/lastvisit/');
	}
	/**
	* 4.3. Запрос системных настроек ЛК
	*
	* @return array
	*/
	final public function InfoOrgSettings(){
		return $this->Get('/info/organization/settings/');
	}
	/**
	* 4.4. Запрос информационных сообщений для организации
	*
	* @return array
	*/
	final public function InfoOrgNotifications(){
		return $this->Get('/info/organization/notifications/');
	}
	/**
	* 4.5. Запрос на получение списка используемых полей
	*
	* @return array
	*/
	final public function InfoOrgFields(){
		return $this->Get('/info/organization/fields/');
	}
	/**
	* 4.6. Запрос на получение списка рассылки для уведомления о платежах
	*
	* @return array
	*/
	final public function InfoOrgReportEmails(){
		return $this->Get('/info/organization/reportemails/');
	}
	/**
	* 4.7. Запрос на получение списка пользователей и их настроек
	*
	* @return array
	*/
	final public function InfoOrgUsers(){
		return $this->Get('/info/organization/users/');
	}
	/**
	* 5.1. Запрос на изменение системных параметров
	*
	* @param array
	* @return array
	*/
	final public function ChangeOrgSetting($dataPost){
		return $this->Post('/change/organization/setting/', $dataPost);
	}
	/**
	* 5.2. Запрос на изменение поля
	*
	* @param array
	* @return array
	*/
	final public function ChangeOrgFormField($dataPost){
		return $this->Post('/change/organization/formfield/', $dataPost);
	}
	/**
	* 5.3. Запрос на добавление адреса в список рассылки уведомлений о принятых платежах
	*
	* @param string $email
	* @return array
	*/
	final public function ChangeOrgAddReportEmail($email){
		return $this->Post('/change/organization/addreportemail/', array('email' => $email));
	}
	/**
	* 5.4. Запрос на удаление адреса из списка рассылки уведомлений о принятых платежах
	*
	* @param string
	* @return array
	*/
	final public function ChangeOrgDeleteReportEmail($idEmail){
		return $this->Post('/change/organization/deletereportemail/', array('id' => $idEmail));
	}
	/**
	* 5.6. Запрос на добавление пользователя
	*
	* @param array
	* @return array
	*/
	final public function ChangeUserAdd($dataPost){
		return $this->Post('/change/user/add/', $dataPost);
	}
	/**
	* 5.7. Запрос на изменение существующего пользователя
	*
	* @param array
	* @return array
	*/
	final public function ChangeUserUpdate($dataPost){
		return $this->Post('/change/user/update/', $dataPost);
	}
	/**
	* 5.8. Запрос на удаление пользователя
	*
	* @param string
	* @return array
	*/
	final public function ChangeUserDelete($idUser){
		return $this->Post('/change/user/delete/', array('id' => $idUser));
	}
	/**
	* 0.0. получение первого чека по ID платежа
	*
	* @param string
	* @return array
	*/
	final public function FirstReceiptByPaymentId($paymentId){
		$fop_receipt_key = $this->OptionsById($paymentId)['fop_receipt_key'];
		return 'https://'.$this->server.'/receipt/'.$paymentId.'/'.$fop_receipt_key;
	}
	/**
	* 7.1. Запрос экспорта истории платежей
	*
	* @param array array('status' => array('','',''), 'form' => '', и т.д);
	* @return array
	*/
	final public function ExportPayments($dataGet){
		return $this->Get('/export/payments/', $dataGet);
	}
	/**
	* 8.3. Получение чеков по платежу.
	* возвращает NULL или ничего, если у платежа нет чеков и
	* не возвращает первый чек
	*
	* @param string ID платежа
	* @return array|NULL
	*/
	final public function InfoReceiptsByPaymentId($paymentId){
		return $this->Get('/info/receipts/bypaymentid/', array('payment_id' => $paymentId));
	}
	/**
	* 8.4. Получение чека по его идентификатору(именно идент. чека не платежа!)
	*
	* @param string ID чека (внутренний номер)
	* @return array|NULL
	*/
	final public function InfoReceiptByReceiptId($receipt_id){
		return $this->Get('/info/receipts/byid/', array('id' => $receipt_id));
	}
	/**
	* 8.11. Сгенерировать чек по платежу или с произвольными атрибутами.
	*
	* @param array $dataPost Массив данных пост запроса
	* @return array
	*/
	final public function ChangeReceiptPrint($dataPost){
		return $this->Post('/change/receipt/print/', $dataPost);
	}
	/**
	* 9.6. Безакцептное списание по имени связки
	*
	* @param array
	* @return array
	*/
	final public function ChangeBindingExecute($dataPost){
		return $this->Post('/change/binding/execute/', $dataPost);
	}
}

?>