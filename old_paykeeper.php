<?php

class PayKeeper{

	private $login = null;
	private $password = null;
	private $keyAuthBasic = null;
	private $server = null;
	private $token = null;
	private $log = array();
	private $debug;

	# инициализируем данные
	public function __construct($login, $password, $server, $debug = FALSE){
		# регистрируем обработчик ошибок
		if($debug){
			register_shutdown_function(array($this, 'getLog'));
			$this->debug = $debug;
		}
		# проверяем и инициализируем данные
		if(is_string($login) && $login != '' && is_string($password) && $password != ''){
			$this->login = $login;
			$this->password = $password;
			# получаем ключ для Basic авторизации запросов
			$this->keyAuthBasic = base64_encode("$login:$password");
		}else{
			$this->addLog(__METHOD__.':Ошибка входных данных! Неверный формат: $login или $password!');
		}
		if(is_string($server) && $server != ''){
			$this->server = $server;
		}else{
			$this->addLog(__METHOD__.':Ошибка входных данных! Неверный формат: $server!');
		}
	}
	# 6.1. Получение токена безопасности
	# нужен только для POST запросов
	private function getToken(){
		$this->addLog('getToken','Получение токена...');
		if(is_null($this->token)){
			# отправляем GET запрос на токен
			$php_array = $this->Get('/info/settings/token/');
			if (isset($php_array['token'])){
				$this->token = $php_array['token'];
				$this->addLog(__METHOD__.':Токен получен ... OK');
			}else{
				$this->addLog(__METHOD__.':Ошибка! Параметр token не получен!');
				return FALSE;
			}
		}
		return $this->token;
	}
	# POST запрос к ЛК PayKeeper
	# вощвращает JSON
	private function Post($uri, $data_post = array()){
		$this->addLog(__METHOD__.':Отправка POST запроса...');
		# проверяем Basic ключ
		if(is_null($this->keyAuthBasic)){
			$this->addLog(__METHOD__.':Ошибка запроса! Basic ключ пуст!');
			return FALSE;
		}
		# подготавливаем заголовки
		$headers = array();
		array_push($headers,'Content-Type: application/x-www-form-urlencoded');
		array_push($headers,'Authorization: Basic '.$this->keyAuthBasic);
		# проверяем и получаем токен для POST запроса
		$token = $this->getToken();
		if(is_null($token) || $token == ''){
			$this->addLog(__METHOD__.':Ошибка запроса! Не удалось получить токен!');
			return FALSE;
		}
		# подготавливаем данные для post-запроса
		$data = http_build_query(array_merge($data_post, array('token' => $token)));
		$options = array("http" => array(
			"method" => "POST",
			"header" => $headers,
			"content" => $data
		));
		$context = stream_context_create($options);
		$out = file_get_contents('https://'.$this->server.$uri, FALSE, $context);
		$this->addLog(__METHOD__.":Ответ сервера ...:".$out);
		# возвращаем результат в массиве
		return json_decode($out, TRUE);
	}
	# GET запрос к ЛК PayKeeper
	# возвращает JSON
	private function Get($uri, $data_get = array()){
		$this->addLog(__METHOD__.':Отправка GET запроса...');
		# проверяем Basic ключ
		if(is_null($this->keyAuthBasic)){
			$this->addLog(__METHOD__.':Ошибка запроса! Basic ключ пуст!');
			return FALSE;
		}
		# подготавливаем заголовки
		$headers = array();
		array_push($headers,'Content-Type: application/x-www-form-urlencoded');
		array_push($headers,'Authorization: Basic '.$this->keyAuthBasic);
		# подготавливаем данные для get-запроса
		if(count($data_get) == 0){
			$data = '';
		}else{
			$data = '?'.http_build_query($data_get);
		}
		# подготавливаем запрос
		$options = array("http" => array(
			"method" => "GET",
			"header" => $headers,
			"content" => $data
		));
		$context = stream_context_create($options);
		$out = file_get_contents('https://'.$this->server.$uri.$data, FALSE, $context);
		$this->addLog(__METHOD__.":Ответ сервера ...:".$out);
		# возвращаем результат в массиве
		return json_decode($out, TRUE);
	}

	private function addLog($massage){
		if($this->debug){
			$this->log['stack'][] = $massage;
		}
	}
	# возвращает массив с ошибками
	public function getLog(){
		# проверяем вкл. ли режим debug
		if($this->debug){
			ob_end_clean();
			$this->log['login'] = $this->login;
			$this->log['password'] = $this->password;
			$this->log['keyAuthBasic'] = $this->keyAuthBasic;
			# появляется только после POST запроса
			$this->log['token'] = $this->token;
			$this->log['server'] = $this->server;
			if(php_sapi_name() == 'cli'){

	    		$result = print_r($this->log, 1);
	    		echo PHP_EOL.$result.PHP_EOL;

	    	}else{
	    		$result = print_r($this->log, 1);
	    		echo '<br/><pre>'.$result.'</pre><br/>';
	    	}
    	}
	}
	# 1.1. Запрос получения списка платежных систем
	public function SystemsList(){
		return $this->Get('/info/systems/list/');
	}

	# 2.9. Запрос на сброс счетчика повторов для платежа
	# Его можно сбросить если он был установлен, т.е. он должен был хоть раз запуститься
	# у него должно появится это значение в параметрах платежа
	public function PaymentRepeatCnt($payment_id){
		return $this->Post('/change/payment/repeatcnt/', array( 'id' => $payment_id ));
	}
	# 2.8. Запрос на возврат платежа
	# возвращает массив
	public function PaymentRevers($payment_id, $amount, $partial, $refund_cart){
		return $this->Post('/change/payment/reverse/', array( 'id' => $payment_id,
			'amount' => $amount,
			'partial' => $partial,
			'refund_cart' => $refund_cart
		));
	}
	# 2.7. Запрос информации по возвратам для платежа
	# возвращает NULL или ничего, если возвратов не было
	public function RefundsByPaymentId($payment_id){
		return $this->Get('/info/refunds/bypaymentid/', array('id' => $payment_id));
	}
	# 2.6. Запрос получения HTTP параметров платежа
	# возвращает массив
	public function HttpLogById($payment_id){
		return $this->Get('/info/httplog/byid/', array('id' => $payment_id));
	}
	# 2.5. Запрос получения дополнительных необязательных параметров платежа
	public function ParamsById($payment_id){
		return $this->Get('/info/params/byid/', array('id' => $payment_id));
	}
	# 2.4. Запрос получение дополнительной информации об опциях
	public function OptionsById($payment_id){
		return $this->Get('/info/options/byid/', array('id' => $payment_id));
	}
	# 2.3. Запрос получения информации о платеже по идентификатору
	# возвращает массив
	public function PaymentById($payment_id){
		return $this->Get('/info/payments/byid/', array('id' => $payment_id));
	}
	# 3.1. Запрос получения данных счёта
	# возвращает массив
	public function InvoiceById($invoice_id){
		return $this->Get('/info/invoice/byid/', array('id' => $invoice_id));
	}
	# 3.2. История счетов(список счетов за указанный период)
	# для параметра status, если значений его много, то
	# можно передать в виде массива:
	# array('status' => array('','',''), 'form' => '', и т.д);
	public function InvoiceList($data_get = array()){
		return $this->Get('/info/invoice/list/', $data_get);
	}
	# 3.3. История счетов(возвращает количество счетов за указанный период)
	# для параметра status, если значений его много, то
	# можно передать в виде массива:
	# array('status' => array('','',''), 'form' => '', и т.д);
	public function InvoiceListСount($data_get = array()){
		return $this->Get('/info/invoice/listcount/', $data_get);
	}
	# 3.4. Подготовка счёта(создание) Для создания и отправки
	# два разных метода
	public function InvoicePreview($data_post = array()){
		return $this->Post('/change/invoice/preview/', $data_post);
	}
	# 3.5. Отправка счёта клиенту
	public function InvoiceSend($invoice_id){
		return $this->Post('/change/invoice/send/', array('id' => $invoice_id));
	}
	# 4.2. Запрос информации о последнем посещении пользователя
	# возвращает массив
	public function LastVisit(){
		return $this->Get('/info/user/lastvisit/');
	}
	# 4.3. Запрос настроек
	# возвращает массив
	public function OrganizationSettings(){
		return $this->Get('/info/organization/settings/');
	}
	# получение первого чека по ID платежа
	public function GetFirstReceiptByPaymentId($payment_id){
		$fop_receipt_key = $this->OptionsById($payment_id)['fop_receipt_key'];
		return 'https://'.$this->server.'/receipt/'.$payment_id.'/'.$fop_receipt_key;
	}
	# 8.3. Получение чеков по платежу.
	# возвращает NULL или ничего, если у платежа нет чеков
	public function ReceiptsByPaymentId($payment_id){
		return $this->Get('/info/receipts/bypaymentid/', array('payment_id' => $payment_id));
	}
	# 8.4. Получение чека по его идентификатору(именно идент. чека не платежа!)
	public function ReceiptById($receipt_id){
		return $this->Get('/info/receipts/byid/', array('id' => $receipt_id));
	}
	# 8.11. Сгенерировать чек по платежу или с произвольными атрибутами.
	public function ReceiptPrint($data_post = array()){
		return $this->Post('/change/receipt/print/', $data_post);
	}
}

?>