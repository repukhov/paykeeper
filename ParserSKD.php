<?php

require_once('phpQuery/phpQuery.php');

class ParserSKD{

	private $uSkd = null;
	private $login = null;
	private $password = null;
	private $cookie = 'paykeeper/cookie/cookie';

	function __construct($login, $password, $uSkd){
		$this->login = $login;
		$this->password = $password;
		$this->uSkd = $uSkd;
	}

	public function Post(){
		# подготавливаем заголовки
		$headers = array();
		$dataPost = array();
		array_push($headers,'Content-Type: application/x-www-form-urlencoded');
		//array_push($headers,'Referer: '.'https://backup.paykeeper.ru:8443/support/');
		array_push($headers,'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36');
		//array_push($headers,'Host: backup.paykeeper.ru:8443');

		# подготавливаем данные для post-запроса
		$data = http_build_query(array_merge($dataPost, array('login' => $this->login, 'password' => $this->password)));
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookie);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_MAXREDIRS, 1);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_URL, $this->uSkd);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		//curl_setopt($curl, CURLOPT_NOBODY, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$out = curl_exec($curl);

		$doc = phpQuery::newDocument($out);
		//$dom->find('.table', 2);
		$entry = $doc->find('.table', 2)->find('tr');
		foreach ($entry as $row) {
			$ent = pq($row);
			echo $ent->find('td:eq(1);')->text().'<br/>';
		}

		if(curl_errno($curl)){
    		/*return array(
    			'result' => 'fail',
    			'details' => 'Ошибка curl: ' . curl_error($curl),
    			'headers' => '',
    		);*/
		}

		# возвращаем результат в массиве
		//return $out;
	}

}




?>